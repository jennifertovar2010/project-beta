from django.urls import path
from .views import api_appointment_list, api_show_appointment, api_technician_list


urlpatterns = [
    path("appointments/", api_appointment_list, name="api_appointment_list"),
    path("appointments/<int:id>/", api_show_appointment, name="api_show_appointment"),
    path("technicians/", api_technician_list, name="api_technician_list"),
]

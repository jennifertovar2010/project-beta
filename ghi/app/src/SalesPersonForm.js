import React, {useState, useEffect} from 'react';


function NewSalesPersonForm () {
  const [name, setName] = useState("");
  const [employeeNumber, setEmployeeNumber] = useState("");

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/employees/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
    }
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.name = name;
    data.employee_number = employeeNumber;

    const locationUrl = 'http://localhost:8090/api/employees/';

    const fetchConfig = {
      method: "POST",
      mode: "cors",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    
    if (response.ok) {
      setName('');
      setEmployeeNumber('');
      alert("Sales Person Added")

    }
    else {
        alert("Sales Person submission failed")
    }

  }

  const handleNameChange = async (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleEmployeeNumberChange = async (event) => {
    const value = event.target.value;
    setEmployeeNumber(value);
  }
  
  return (
<div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>New Sales Person</h1>
            <form onSubmit={handleSubmit} id="create-A-Sales Person">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name}/>
                <label htmlFor="name">Name</label>
                </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmployeeNumberChange} placeholder="employeenumber" required type="employeenumber" name="employeenumber" id="employeenumber" className="form-control" value={employeeNumber}/>
                <label htmlFor="name">Employee Number</label>
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
            </form>
            </div>
            </div>
        </div>
    
            );
        }

export default NewSalesPersonForm;
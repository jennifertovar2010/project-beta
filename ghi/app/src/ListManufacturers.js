import React, {useState, useEffect} from "react";


function ManufacturerList(props) {
    const [manufacturer, setManufacturers] = useState([])

    useEffect(()=>{
        const getFetch = async () => {
            const url = 'http://localhost:8100/api/manufacturers/';
            const response = await fetch(url);
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
        getFetch()
    }, []);
 
  return (
    <>

      <div className="container">
        <table className="table table-striped">
          <thead>
            <tr>
            <th>Manufacturer</th>
            <th></th>
            </tr>
          </thead>
          <tbody>
            {manufacturer.map(manufacturer => {
              return (
                <tr key={manufacturer.id}>
                  <td>{manufacturer.name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default ManufacturerList;
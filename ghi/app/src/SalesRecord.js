import React, {useState, useEffect} from 'react';


function NewSalesForm () {
  const [automobile, setAutomobile] = useState("");
  const [salesPerson, setsalesPerson] = useState("");
  const [customer, setCustomer] = useState("");
  const [automobileList, setAutomobileList] = useState([]);
  const [salesPersonList, setsalesPersonList] = useState([]);
  const [customerList, setCustomerList] = useState([]);
  const [price, setprice] = useState("");
  

  const fetchData = async () => {
    const automobileUrl = 'http://localhost:8100/api/automobiles/';
    const automobileResponse = await fetch(automobileUrl);

    if (automobileResponse.ok) {
      const data = await automobileResponse.json();
      setAutomobileList(data.autos);
    }
  }

  const fetchDataPerson = async () => {
    const salesPersonUrl = 'http://localhost:8090/api/employees/';
    const salesPersonResponse = await fetch(salesPersonUrl);

    if (salesPersonResponse.ok) {
      const data = await salesPersonResponse.json();
      setsalesPersonList(data.sales_person);
    }
  }

  const fetchDataCustomer = async () => {
    const customerUrl = 'http://localhost:8090/api/customers/';
    const customerResponse = await fetch(customerUrl);

    if (customerResponse.ok) {
      const data = await customerResponse.json();
      setCustomerList(data.customers);
    }
  }

  useEffect(()=> {
    fetchData();
    fetchDataPerson();
    fetchDataCustomer();
  }, [])

  const handleSubmit = async (event) => {
    event.preventDefault();
    
    const data = {};
    
    data.price = Number(price);
    data.sales_person = Number(salesPerson);
    data.customer = Number(customer);
    data.automobile = Number(automobile);

    const locationUrl = 'http://localhost:8090/api/sales/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    
    if (response.ok) {
      setAutomobile('');
      setsalesPerson('');
      setCustomer('');
      setprice('');
      alert("Sale Added")
    }
    else {
        alert("Sale Not added")
    }
  }
  
  return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>New Sale</h1>
            <form onSubmit={handleSubmit} id="create-A-Customer">
              <div className="form-floating mb-3">
                <select value={automobile} id="auto" onChange={(event)=>setAutomobile(event.target.value)} name="name" className="form-control"> 
                <option value="">Choose an automobile</option>
                {automobileList.map(auto=>{
                    return <option value={auto.id} key={auto.id}>{auto.year} {auto.color} {auto.model.manufacturer.name} {auto.model.name}</option>
                })}
                </select>
                <label htmlFor="auto">Automobile</label>
                </div>
                <div className="form-floating mb-3">
                  <select value={salesPerson} id="salesPerson" onChange={(event)=>setsalesPerson(event.target.value)} name="salesPerson" className="form-control">
                  <option value="">Choose a Sales Person</option>
                  {salesPersonList.map(employee=>{
                     return <option value={employee.id} key={employee.id}>{employee.name} {employee.employee_number}</option>
                  })}
                  </select>
                  <label htmlFor="salesPerson">Sales Person</label>
                </div>
                <div className="form-floating mb-3">
                  <select value={customer} id="customer" onChange={(event)=>setCustomer(event.target.value)} name="customer" className="form-control">
                  <option value="">Choose a Customer</option>
                  {customerList.map(customer=>{
                     return <option value={customer.id} key={customer.id}>{customer.name} {customer.address} {customer.phone_number} </option>
                  })}
                  </select>
                  <label htmlFor="customer">Customer</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={(event)=>setprice(event.target.value)} placeholder="price" required type="number" min="0" name="price" id="price" className="form-control" value={price}/>
                <label htmlFor="price">Price</label>
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
            </form>
            </div>
            </div>
        </div>
    
            );
        }

export default NewSalesForm;
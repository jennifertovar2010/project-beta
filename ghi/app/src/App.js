import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import NewSalesPersonForm from './SalesPersonForm';
import ListSalesPersonHistory from './SalesPersonRecord';
import NewSaleForm from './SalesRecord';
import NewManufacturerForm from './ManufacturerForm';
import SalesList from './SalesList';
import NewCustomerForm from './CustomerForm';
import TechnicianForm from './TechnicianForm';
import ManufacturerList from './ListManufacturers';
import ServiceAppointment from './ServiceAppointment';
import NewModelForm from './ModelForm';
import ModelList from './ModelList';
import ServiceHistory from './ServiceHistory';
import ListAutomobiles from './AutomobileInventory';
import NewAutoForm from './AutomobileForm';
import ServiceAppointmentList from './ServiceAppointmentList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/sales-person" element={<NewSalesPersonForm />} />
          <Route path="/sales-person-history" element={<ListSalesPersonHistory />} />
          <Route path="/new-sale" element={<NewSaleForm />} />
          <Route path="/new-manufacturer" element={<NewManufacturerForm />} />
          <Route path="/list-sales" element={<SalesList />} />
          <Route path="/customers" element={<NewCustomerForm />} />
          <Route path="/new-technician" element={<TechnicianForm />} />
          <Route path="/manufacturers" element={<ManufacturerList />} />
          <Route path="/new-service-appointment" element={<ServiceAppointment />} />
          <Route path="/new-model" element={<NewModelForm />} />
          <Route path="/models" element={<ModelList />} />
          <Route path="/service-history" element={<ServiceHistory />} />
          <Route path="/automobiles" element={<ListAutomobiles/>} />
          <Route path="/automobiles-new" element={<NewAutoForm/>} />
          <Route path="/service-appointment-list" element={<ServiceAppointmentList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

import React, {useState, useEffect} from "react";


function ModelList(props) {
    const [models, setModels] = useState([])

    useEffect(()=>{
        const getFetch = async () => {
            const url = 'http://localhost:8100/api/models/';
            const response = await fetch(url);
            const data = await response.json()
            setModels(data.models)
        }
        getFetch()
    }, []);

    const deleteModel=async (id) => {
        const response= await fetch('http://localhost:8100/api/models/', {
            method: 'DELETE',
            mode: "cors",
            headers: {
                "Content-Type": "application/json"
            }
        })
        const data = await response.json()
            setModels(
            models.filter((models) =>{
                return models.id !== id;
            })
        )
    }
 
  return (
    <>

      <div className="container">
        <table className="table table-striped">
          <thead>
            <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
            </tr>
          </thead>
          <tbody>
            {models.map(model => {
              return (
                <tr key={model.id}>
                  <td>{model.name}</td>
                  <td>{model.manufacturer.name}</td>
                  <td>
                    <img src= {model.picture_url} alt= {model.name}/>
                  </td>

                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default ModelList;
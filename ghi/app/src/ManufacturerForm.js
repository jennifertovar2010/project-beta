import React, {useState, useEffect} from 'react';


function NewManufacturerForm () {
  const [manufacturers, setManufacturers] = useState([]);
  const [name, setName] = useState("");

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  }

  useEffect(()=> {
    fetchData();
  }, [])

  const handleSubmit = async (event) => {
    event.preventDefault();
    
    const data = {};

    data.name = name;

    const locationUrl = 'http://localhost:8100/api/manufacturers/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    
    if (response.ok) {
      setName('');
      alert("Manufacturer Added")
    }
  }

  const handleNameChange = async (event) => {
    const value = event.target.value;
    setName(value);
  }
  
  return (
<div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-A-Manufacturer">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="style" className="form-control" value={name}/>
                <label htmlFor="name">Name</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
            </div>
        </div>
    
            );
        }

export default NewManufacturerForm;
import React, { useState } from "react";


const TechnicianForm = () => {
    const [name, setName] = useState("");
    const [employeeNumber, setEmployeeNumber] = useState("");

    const handleSubmit = async (e) => {
        e.preventDefault();
        const requestbody = {
            technician_name: name,
            employee_number: employeeNumber
        };
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(requestbody),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch("http://localhost:8080/api/technicians/", fetchConfig)
        const data = await response.json();

        setName("");
        setEmployeeNumber("");
        alert("Technician Added");
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-A-Technician">
                        <div className="form-floating mb-3">
                            <input onChange={(e) => setName(e.target.value)} placeholder="Name" required type="text" name="name" id="style" className="form-control" value={name} />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => setEmployeeNumber(e.target.value)} placeholder="Employee Number" required type="text" name="EmployeeNumber" id="style" className="form-control" value={employeeNumber} />
                            <label htmlFor="name">Employee Number</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default TechnicianForm

import React, {useState, useEffect} from 'react';


function NewModelForm () {
  const [name, setName] = useState("");
  const [picture, setpicture] = useState("");
  const [manufacturer, setManufactuer] = useState("");
  const [manufacturerList, setmanufacturerist] = useState([]);

  

  const fetchData = async () => {
    const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
    const manufacturerResponse = await fetch(manufacturerUrl);

    if (manufacturerResponse.ok) {
      const data = await manufacturerResponse.json();
      setmanufacturerist(data.manufacturers);
    }
  }


  useEffect(()=> {
    fetchData();
  }, [])

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.name = (name);
    data.picture_url = (picture);
    data.manufacturer_id = Number(manufacturer);

    const locationUrl = 'http://localhost:8100/api/models/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    console.log(data);
    
    if (response.ok) {
      setName('');
      setpicture('');
      setManufactuer('');
      alert("Model Added")
    }
    else {
        alert("Model Not added")
    }
  }
  
  return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Vehicle Model</h1>
            <form onSubmit={handleSubmit} id="create-A-Model">
              <div className="form-floating mb-3">
                <input value= {name} id="name" onChange={(event)=>setName(event.target.value)} name="name" className="form-control"/>
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={picture} id="picture" onChange={(event)=>setpicture(event.target.value)} name="picture" className="form-control"/>

                  <label htmlFor="picture">Picture Url</label>
                </div>
                <div className="form-floating mb-3">
                  <select value={manufacturer} id="manufacturer" onChange={(event)=>setManufactuer(event.target.value)} name="manufacturer" className="form-control">
                  <option value="">Choose a manufacturer</option>
                  {manufacturerList.map(manufacturer=>{
                     return <option value={manufacturer.id} key={manufacturer.id}>{manufacturer.name} </option>
                  })}
                  </select>
                  <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
            </form>
            </div>
            </div>
        </div>
    
            );
        }

export default NewModelForm;
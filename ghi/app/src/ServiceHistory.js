import React, { useState, useEffect } from "react";


const ServiceHistory = () => {
    const [filterValue, setFilterValue] = useState("");
    const [appointments, setAppointments] = useState([]);
    const fetchData = async () => { 
        const appointmentUrl = "http://localhost:8080/api/appointments/";
        const response = await fetch(appointmentUrl);

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const handleChange = (e) => {
        setFilterValue(e.target.value.toLowerCase());
    };

    const filteredAppointments = (e) => {
        if (filterValue === "") {
            return [];
        } else {
            return appointments.filter((appointment) =>
                appointment["vin"].toLowerCase().includes(filterValue) || appointment["customer_name"].toLowerCase().includes(filterValue)
            );
        };
    };
    return (
        <div className="container">
            <div className="form-floating mb-3">
                <input onChange={handleChange} placeholder="Search Vin Or Customer Name" required type="text" name="filterValue" id="style" className="form-control" value={filterValue} />
                <button>Search Name or Vin</button>
            </div>
            <h1>Service History</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredAppointments().map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer_name}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.technician.technician_name}</td>
                                <td>{appointment.reason}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default ServiceHistory;

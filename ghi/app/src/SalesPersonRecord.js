import React, {useState, useEffect} from "react";


function ListSalesPersonHistory(props) {
    const [sales, setSales] = useState([])
    const [employees, setEmployees] = useState([])
    const [employeeNumber, setEmployeeNumber] = useState("")

    useEffect(()=>{
        const getFetch = async () => {
            const url = 'http://localhost:8090/api/sales/';
            const response = await fetch(url);
            const data = await response.json()
            setSales(data.sales)
            const saleUrl= 'http://localhost:8090/api/employees/'
            const saleresponse = await fetch(saleUrl);
            const saledata = await saleresponse.json()
            setEmployees(saledata.sales_person)

        }
        getFetch()
    }, []);
    const filterSales=employeeNumber ? sales.filter(sale=>sale.sales_person.employee_number==employeeNumber) : sales

 
  return (
    <>

      <div className="container">
        <select value={employeeNumber} onChange={e => setEmployeeNumber(e.target.value)} >
            <option value="">Choose a sales person</option>
            {employees.map(person =><option key={person.employee_number} value= {person.employee_number}>{person.name}</option>)}
        </select>
        <table className="table table-striped">
          <thead>
            <tr>
            <th>Sales Person</th>
                <th>Customer</th>
                <th>Vin</th>
                <th>Price</th>
                <th></th>
            </tr>
          </thead>
          <tbody>
            {filterSales.map(sale => {
              return (
                <tr key={sale.id}>
                <td>{sale.sales_person.name}</td>
                    <td>{sale.customer.name}</td>
                    <td>{sale.automobile.vin}</td>
                    <td>{sale.price}</td>


                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default ListSalesPersonHistory;
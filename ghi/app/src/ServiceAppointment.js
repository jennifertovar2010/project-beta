import React, { useState, useEffect } from "react";


const ServiceAppointment = () => {
    const [custName, setCustName] = useState("");
    const [vinNumber, setVinNumber] = useState("");
    const [apptDate, setApptDate] = useState("");
    const [apptTime, setApptTime] = useState("");
    const [assignedTechnician, setAssignedTechnician] = useState("");
    const [technicianList, setTechnicianList] = useState([]);
    const [appointmentReason, setAppointmentReason] = useState("");

    const fetchData = async () => {
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const technicianResponse = await fetch(technicianUrl);

        if (technicianResponse.ok) {
            const data = await technicianResponse.json();
            console.log(data);
            setTechnicianList(data.technician);
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};

        data.vin = vinNumber;
        data.customer_name = custName;
        data.date = apptDate;
        data.time = apptTime;
        data.technician = Number(assignedTechnician);
        data.reason = appointmentReason;

        const locationUrl = 'http://localhost:8080/api/appointments/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig)

        if (response.ok) {
            setCustName("");
            setVinNumber("");
            setApptDate("");
            setApptTime("");
            setAssignedTechnician("");
            setAppointmentReason("");
            alert("Service Appointment Created");
        }
        else {
            alert("Service Appointment Not Added");
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Service Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-An-Appointment">
                        <div className="form-floating mb-3">
                            <input onChange={(e) => setCustName(e.target.value)} placeholder="Cust Name" required type="text" name="cust_name" id="style" className="form-control" value={custName} />
                            <label htmlFor="name">Customer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => setVinNumber(e.target.value)} placeholder="Vin Number" required type="text" name="vinNumber" id="style" className="form-control" value={vinNumber} />
                            <label htmlFor="name">Vin Number</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => setApptDate(e.target.value)} placeholder="Appt Date" required type="date" name="apptDate" id="style" className="form-control" value={apptDate} />
                            <label htmlFor="name">Select A Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => setApptTime(e.target.value)} placeholder="Appt Time" required type="time" name="apptTime" id="style" className="form-control" value={apptTime} />
                            <label htmlFor="name">Select A Time</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select value={assignedTechnician} onChange={(e)=>setAssignedTechnician(e.target.value)} required name="technician" id="technician" className="form-select">
                                <option value="">Assign a Technician</option>
                                {technicianList.map(technician => {
                                    return (
                                        <option key={technician.employee_number} value={technician.id}>{technician.technician_name}</option>
                                    );
                                })}
                            </select>                        
                            <label htmlFor="name">Technician Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => setAppointmentReason(e.target.value)} placeholder="Appointment Reason" required type="text" name="appointmentReason" id="style" className="form-control" value={appointmentReason} />
                            <label htmlFor="name">Reason for Appointment</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ServiceAppointment
import React, {useState, useEffect} from "react";


function ListAutomobiles(props) {
    const [automobiles, setAutomobiles] = useState([])

    useEffect(()=>{
        const getFetch = async () => {
            const url = 'http://localhost:8100/api/automobiles/';
            const response = await fetch(url);
            const data = await response.json()
            setAutomobiles(data.autos)
        }
        getFetch()
    }, []);

    const deleteAutomobile=async (id) => {
        const response= await fetch(`http://localhost:8100/api/automobiles/`, {
            method: 'DELETE',
            mode: "cors",
            headers: {
                "Content-Type": "application/json"
            }
        })
        
        const data = await response.json()
            setAutomobiles(
            automobiles.filter((sale) =>{
                return sale.id !== id;
            })
        )
    }
 
  return (
    <>

      <div className="container">
        <table className="table table-striped">
          <thead>
            <tr>
                <th>Vin</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
                <th></th>
            </tr>
          </thead>
          <tbody>
            {automobiles.map(automobile => {
              return (
                <tr key={automobile.id}>
                    <td>{automobile.vin}</td>
                <td>{automobile.color}</td>
                    <td>{automobile.year}</td>
                    <td>{automobile.model.name}</td>
                    <td>{automobile.model.manufacturer.name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default ListAutomobiles;
import React, {useState, useEffect} from 'react';


function NewCustomerForm () {
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [phone_number, setPhone_number] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.name = name;
    data.address = address;
    data.phone_number = phone_number;

    const locationUrl = 'http://localhost:8090/api/customers/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    
    if (response.ok) {
      setName('');
      setAddress('');
      setPhone_number('');
      alert("Customer Added")
    }
    else {
        alert("Customer Not added")
    }
  }

  const handleNameChange = async (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleAddressChange = async (event) => {
    const value = event.target.value;
    setAddress(value);
  }
  
  const handlePhoneNumberChange = async (event) => {
    const value = event.target.value;
    setPhone_number(value);
  }
  
  return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Customer</h1>
            <form onSubmit={handleSubmit} id="create-A-Customer">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="name" required type="name" name="name" id="name" className="form-control" value={name}/>
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleAddressChange} placeholder="Address" required type="address" name="address" id="address" className="form-control" value={address} />
                  <label htmlFor="address">Address</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePhoneNumberChange} placeholder="phone number" required type="phone number" name="phone number" id="phone number" className="form-control" value={phone_number} />
                  <label htmlFor="phone number">Phone Number</label>
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
            </form>
            </div>
            </div>
        </div>
    
            );
        }

export default NewCustomerForm;
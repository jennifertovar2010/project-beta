import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <NavLink className="nav-link" to="/new-manufacturer">Create Manufacturer</NavLink>
            <NavLink className="nav-link" to="/manufacturers">All Manufacturers</NavLink>
            <NavLink className="nav-link" to="/new-sale">Create a sale record</NavLink>
            <NavLink className="nav-link" to="/list-sales">All Sales</NavLink>
            <NavLink className="nav-link" to="/sales-person">New Sales Person</NavLink>
            <NavLink className="nav-link" to="/sales-person-history">Sales Person History</NavLink>
            <NavLink className="nav-link" to="/customers">New Customer</NavLink>
            <NavLink className="nav-link" to="/new-technician">New Technician</NavLink>
            <NavLink className="nav-link" to="/new-service-appointment">New Service Appointment</NavLink>
            <NavLink className="nav-link" to="/service-history">Service History</NavLink>
            <NavLink className="nav-link" to="/service-appointment-list">Service Appointment List</NavLink>
            <NavLink className="nav-link" to="/new-model">Create a Model</NavLink>
            <NavLink className="nav-link" to="/models">All Models</NavLink>
            <NavLink className="nav-link" to="/automobiles">All Automobiles</NavLink>
            <NavLink className="nav-link" to="/automobiles-new">Add Automobiles</NavLink>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;

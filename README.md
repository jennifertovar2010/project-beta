# CarCar

Team:

* Person 1 - Jennifer Tovar - Service
* Person 2 - Gabby Tietjen - Sales microservice.

## Design

CarCar is a webpage that we built using react along with django in the python language.  We have a navbar with NavLinks to each service and the history of that service that we offer.  We used a combination of CSS, bootstrap, html, JSX to create and style our forms.  The filter method is used on the sales persons history, and on the service history form.  This allows us to look a single sales persons history of sold cars.  We also can see all the service appointments that one specific vin has had.

## Service microservice

Car Car is a full stack web service that was built with both django and React. Django was used to build the backend and React was used to build the frontend. Docker is used to build the containers and images and a poller is used to update the inventory in the API.

I was responsible for the service part in this project. Let's begin with the backend. For the backend, I created two models and a value object model. The two models I created are Technician and Appointment models and the value object model that I created is the AutomobileVO. The AutomobileVO model contains the vin and the href. Those were the only two data fields that was needed to pull from the Inventory Automobile model. The Technician model holds the technician name and employee number. I made both fields to be a CharField so that employers can add upto 100 characters to the name and employee number. The Appointment model holds a few fields: the owner of vehicle = customer name; date & time of the appointment; the reason for the appointment; the technician assigned to the appointment; the vin number; vip status; and the status of the appointment. I made all fields to be a CharField except for the vip status and the technician field.I used a foreign key for the technician to pull the information from the Technician model. I made my on_delete to equal Protect because I wanted to protect the appointments that were assigned to that particular technician incase the technician was removed from the database. I made the vip status a BooleanField to let us know if the vehicle is apart of the inventory and if it is, the technician will know to give that customer vip treatment. My poller was created via an API call to the inventory service to fetch all autombiles as they were generated either in Insomnia or via our completed React forms.

All of my Encoders that were created for my views, are kept in a separate encoder file. I created my views using the CRUD structure. The views I created deals with creating or modifying technicians and appointments. A GET request is used to view all instances; a POST request is used to create an instance using the data from a form that matches my models; a GET request passed with an ID is used to view an instance with that ID; a PUT request is used to update any information; and a DELETE request is used to delete an instance you want to delete. Error messages were handled in Try or Except statements when necessary, such as when fetching an instance object of a technician or appointment.

Now lets talk about the front end. The service microservice allows you to create a service appointment, create a technician, view the service appointment history and view the list of appointments. There were several React components needed to be created for the Service portion of the project. A form was created to create a new technician. A basic form template was rendered, and a handleSubmit function created the new technician instance by posting the request to the correct API url. The same strategy was used to create an appointment, except that the technician had to be fetched in the useEffect method to populate a dropdown. All the other fields are input fields. In the appointment list, a table was created to display the data fetched from the API. Functionality was created to delete an appointment. The service appointment history view was created using a table like the one used in the listing of all appointments. The only difference between the tables was the buttons to cancel or finish an appointment. I made the view to filter the data by VIN number and customer name to make it easier for the technician to look up the history of appointments.  


## Sales microservice

Car Car is a full stack web application built with Django for the backend and React for the frontend.  Docker is used to build and keep the containers, images and a poller is used to update our inventory API.

For the sales microservice I created four models, SalesPerson, Customer, AutomobileVO and SalesRecord.  SalesPerson holds the employees name and employee number. Customer holds the customers name, address, and phone number. AutomobileVO creates the link to bring vin into my microservice from the inventory microservice.  SalesRecord holds everything about a sale, the price, customer, sales person, and the vin.

On the sales microservice you can create a sales person, create a customer, create a new sales record all with a form.  You can also view all the sales that have been made, and you can view them filtered by the sales person.

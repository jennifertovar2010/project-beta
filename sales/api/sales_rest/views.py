from django.shortcuts import render
from .models import SalesPerson, Customer, AutomobileVO, SalesRecord
from django.views.decorators.http import require_http_methods
from .encoders import AutomobileEncoder, SalesRecordEncoder, SalesPersonEncoder,CustomerEncoder
import json
from django.http import JsonResponse


@require_http_methods(["GET", "POST", ])
def api_sales_persons(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_person": sales_person},
            encoder=SalesPersonEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Customer already exists"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_customer(request, id):
    if request.method == "GET":
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        (count,) = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        Customer.objects.filter(id=id).update(content)

        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST", "DELETE"])
def api_list_sales(request):
    if request.method == "GET":
        sales = SalesRecord.objects.all()
        return JsonResponse({"sales": sales}, encoder=SalesRecordEncoder)
    elif request.method == "DELETE":
        count, _ = SalesRecord.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        try:
            content = json.loads(request.body)
            automobile = content["automobile"]
            automobile = AutomobileVO.objects.get(id=automobile)
            content["automobile"] = automobile

            sales_person = content["sales_person"]
            sales_person = SalesPerson.objects.get(id=sales_person)
            content["sales_person"] = sales_person

            customer = content["customer"]
            customer = Customer.objects.get(id=customer)
            content["customer"] = customer
            print(content)
            sale = SalesRecord.objects.create(**content)
            print(sale.__dict__)
            return JsonResponse(
                sale,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Automobile Id does not exist"}, status=400)


@require_http_methods(["GET", "DELETE"])
def api_show_sale(request, id):
    if request.method == "GET":
        sale = SalesRecord.objects.get(id=id)
        return JsonResponse(sale, encoder=SalesRecordEncoder, safe=False)
    else:
        (count,) = SalesRecord.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET"])
def api_unsold_vins(request):
    if request.method == "GET":
        sold_auto = [sale.automobile.vin for sale in SalesRecord.objects.all()]
        unsold_auto = AutomobileVO.objects.exclude(vin__in=sold_auto)
        return JsonResponse(
            {"automobiles": unsold_auto}, encoder=AutomobileEncoder, safe=False
        )


@require_http_methods(["GET"])
def api_list_automobilevo(request):
    autos = AutomobileVO.objects.all()
    return JsonResponse(
        {"autos": autos},
        encoder=AutomobileEncoder,
    )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_sales_person(request, id):
    if request.method == "GET":
        sales_person = SalesPerson.objects.get(id=id)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = SalesPerson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        SalesPerson.objects.filter(id=id).update(**content)

        sales_person = SalesPerson.objects.get(id=id)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )

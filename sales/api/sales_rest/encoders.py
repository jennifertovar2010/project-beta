from common.json import ModelEncoder
from .models import SalesPerson, Customer, AutomobileVO, SalesRecord


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number", "id"]


class CustomerEncoder(ModelEncoder):
    model= Customer
    properties = ["name", "address", "phone_number", "id"]


class AutomobileEncoder(ModelEncoder):
    model= AutomobileVO
    properties= ["vin", "id",]


class SalesRecordEncoder(ModelEncoder):
    model= SalesRecord
    properties= ["automobile","sales_person", "customer","price", "id"]
    encoders = {
        "automobile": AutomobileEncoder(),
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
    }
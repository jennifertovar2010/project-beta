from django.urls import path
from .views import (
    api_list_sales,
    api_sales_person,
    api_customer,
    api_show_sale,
    api_customers,
    api_sales_persons,
)

urlpatterns = [
    path(
        "employees/",
        api_sales_persons,
        name="api_sales_persons",
    ),
    path(
        "employees/<int:id>/",
        api_sales_person,
        name="api_sales_person",
    ),
    path(
        "customers/",
        api_customers,
        name="api_customers",
    ),
    path(
        "customers/<int:id>/",
        api_customer,
        name="api_customer",
    ),
    path(
        "sales/",
        api_list_sales,
        name="api_create_sales",
    ),
    path(
        "sales/<int:id>/",
        api_show_sale,
        name="api_show_sale",
    ),
]
